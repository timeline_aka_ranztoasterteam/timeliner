package timeliner;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Spliterator;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import timeliner.db.Post;
import timeliner.db.User;
import timeliner.db.repoPost.PostService;
import timeliner.db.repoThread.ThreadService;
import timeliner.db.repoUser.UserService;
import timeliner.db.Thread;

@Controller
@EntityScan
public class MainController {
	
	private final UserService userService;
	private final ThreadService threadService;
	private final PostService postService;
	
	@Autowired
	public MainController(UserService userService, ThreadService threadService, PostService postService) {
		this.userService = userService;
		this.threadService = threadService;
		this.postService = postService;
	}
	
	
	
	@InitBinder
	public void binder(WebDataBinder binder){
		binder.registerCustomEditor(java.sql.Date.class, new Umwandln());
	}

	final class Umwandln extends PropertyEditorSupport{

		@Override
		public void setAsText(String text) throws IllegalArgumentException {
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate d = LocalDate.parse(text, fmt);
			setValue(java.sql.Date.valueOf(d));
		}
	}
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String getMain() {
		
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null)
			return "redirect:/login";
		else
			return "redirect:/main";
	}
	
	@RequestMapping(value="/title.png", method=RequestMethod.GET)
	public String getMaain() {
		return "redirect:/main";
	}
	
	
	@RequestMapping(value="/error", method=RequestMethod.GET)
	public String getError() {
		return "main";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String gotoMain() {
		return "redirect:/main";
	}

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String getLogin() {
		return "login";
	}
	
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/debugpeople", method=RequestMethod.GET)
	public ModelAndView getUsersForDebug() {
		ModelAndView mav = new ModelAndView("debug");
		Spliterator<User> it = userService.findAll().spliterator();
		mav.addObject("debug", StreamSupport.stream(it, false).toArray());
		return mav;
		
	}
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/debugthreads", method=RequestMethod.GET)
	public ModelAndView getThreadsForDebug() {
		ModelAndView mav = new ModelAndView("debugt");
		Spliterator<Thread> it = threadService.findAll().spliterator();
		mav.addObject("debugt", StreamSupport.stream(it, false).toArray());
		return mav;
	}
	@Secured("ROLE_USER")
	@RequestMapping(value="/main", method=RequestMethod.GET)
	public ModelAndView getSortedMainPage(@RequestParam(required = false) String sort) {
		ModelAndView mav = new ModelAndView("main");
		Spliterator<Thread> it = threadService.findAll().spliterator();
		if(sort == null) {
			mav.addObject("threads", StreamSupport.stream(it, false).toArray());
			mav.addObject("sort", "not sorted");
		}
		else if(sort.equals("date")) {
			mav.addObject("threads", StreamSupport.stream(it, false).sorted((thread1, thread2) -> thread1.getDate().compareTo(thread2.getDate())).toArray());
			mav.addObject("sort", "sorted after date");
		}
			else if(sort.equals("maxcomments")){
			mav.addObject("threads", StreamSupport.stream(it, false).sorted((thread1, thread2) -> Integer.compare(thread1.getPosts().size(), thread2.getPosts().size())).toArray());
			mav.addObject("sort", "sorted after comments");
			}
			return mav;
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value="/main", method=RequestMethod.PUT)
	public ModelAndView getSortedMainPagePUT(@RequestParam(required = false) String sort) {
		ModelAndView mav = new ModelAndView("main");
		Spliterator<Thread> it = threadService.findAll().spliterator();
		if(sort == null) {
			mav.addObject("threads", StreamSupport.stream(it, false).toArray());
			mav.addObject("sort", "not sorted");
		}
		else if(sort.equals("date")) {
			mav.addObject("threads", StreamSupport.stream(it, false).sorted((thread1, thread2) -> thread1.getDate().compareTo(thread2.getDate())).toArray());
			mav.addObject("sort", "sorted after date");
		}
			else if(sort.equals("maxcomments")){
			mav.addObject("threads", StreamSupport.stream(it, false).sorted((thread1, thread2) -> Integer.compare(thread1.getPosts().size(), thread2.getPosts().size())).toArray());
			mav.addObject("sort", "sorted after comments");
			}
			return mav;
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value="/thread/{id}", method=RequestMethod.GET)
	public ModelAndView getThread(@PathVariable String id) {
		ModelAndView mav = new ModelAndView("threads");
		Spliterator<Thread> it = threadService.findAll().spliterator();
		mav.addObject("threads", StreamSupport.stream(it, false).filter(x -> x.getName().equals(id)).toArray());
		mav.addObject("posts", StreamSupport.stream(postService.findAll().spliterator(), false).filter(post -> post.getThread().getName().equals(id)).sorted((x,y) -> x.getDate().compareTo(y.getDate())).toArray());
		
		return mav;
	}
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/debugposts", method=RequestMethod.GET)
	public ModelAndView getPostsForDebug() {
		ModelAndView mav = new ModelAndView("debugp");
		Spliterator<Post> it = postService.findAll().spliterator();
		mav.addObject("html/debugp", StreamSupport.stream(it, false).toArray());
		return mav;
	}
	@Secured("ROLE_USER")
	@RequestMapping(value="/post/add", method=RequestMethod.GET)
	public String addPost(@RequestParam("text") String text, @RequestParam("thread") String thread) {
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User us = userService.find(user.getUsername());
		Post post = new Post(new Date(), text, us, threadService.find(thread), 0, 0);
		postService.save(post);
		return "redirect:/thread/"+thread;
	}
	
	
	@Secured("ROLE_USER")
	@RequestMapping(value="/thread/add", method=RequestMethod.GET)
	public String addThread(@RequestParam("name") String name, @RequestParam("thematik") String thematik) {
		
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User us = userService.find(user.getUsername());
		Thread thread = new Thread(name, thematik, us);
		threadService.save(thread);
		return "redirect:/main";
	}

	@Secured("ROLE_USER")
	@RequestMapping(value="/like/post", method=RequestMethod.GET)
	public String likePost(@RequestParam("post") int id) {
		
		Post post = postService.find(id);
		postService.delete(id);
		post.setLikes(post.getLikes()+1);
		postService.save(post);
		
		return "redirect:/thread/"+post.getThread().getName();
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value="/dislike/post", method=RequestMethod.GET)
	public String dislikePost(@RequestParam("post") int id) {

		Post post = postService.find(id);
		postService.delete(id);
		post.setDislikes(post.getDislikes()+1);
		postService.save(post);
		
		return "redirect:/thread/"+post.getThread().getName();
	}
}
