package timeliner.db;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "post")
public class Post {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "p_id")
	private int id;
	
	@Past
	@DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss")
	@Column(name = "p_date")
	private Date date;

	@Column(name = "p_dislikes")
	private int dislikes;
	
	@Column(name = "p_likes")
	private int likes;

	@NotNull
	@Size(min = 1, max = 1500, message="{validation.post.Length.message}")
	@Column(name = "p_text")
	private String text;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "u_username", nullable = false)
	public User user;
	
//	@Column(name = "p_likes")
//	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "user")
//	private List<User> likes;
//	
//	@Column(name = "p_dislikes")
//	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "user")
//	private List<User> dislikes;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "p_name", nullable = false)
	private Thread thread;
	
	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	private Post()
	{
		
	}
	
	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public Post(Date date, String text, User user, Thread thread, int likes, int dislikes) {
		setDate(date);
		setText(text);
		setUser(user);
		setLikes(likes);
		setDislikes(dislikes);
		setThread(thread);
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getId() {
		return id;
	}
	
	public void setUser(User user)
	{
		this.user = user;
	}
	
	public User getUser()
	{
		return this.user;
	}

	@Override
	public String toString() {
		return "Post [date=" + date + ", text=" + text + ", user=" + user + "]";
	}
	
	
//	public List<User> getLikes() {
//		return likes;
//	}
//
//	public void setLikes(List<User> likes) {
//		this.likes = likes;
//	}
//
//	public List<User> getDislikes() {
//		return dislikes;
//	}
//
//	public void setDislikes(List<User> dislikes) {
//		this.dislikes = dislikes;
//	}
//	

}


