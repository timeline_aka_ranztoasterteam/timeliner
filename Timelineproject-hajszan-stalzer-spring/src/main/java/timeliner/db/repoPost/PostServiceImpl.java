package timeliner.db.repoPost;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import timeliner.db.Post;

@Component("postService")
public class PostServiceImpl implements  PostService {
	private PostRepository postRepository;
	
	@Autowired
	public PostServiceImpl(PostRepository postRepository)
	{
		this.postRepository = postRepository;
	}

	@Override
	public List<Post> findAll() {
		return postRepository.findAll();
	}
	
	@Override
	public Post save(Post entity) {
		return postRepository.save(entity);
	}
	
	@Override
	public void delete(int id) {
		postRepository.delete(id);
	}
	
	@Override
	public Post find(int id) { 
		 return postRepository.findOne(id);
	}
}