package timeliner.db.repoPost;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import timeliner.db.Post;
import timeliner.db.User;

public interface PostRepository extends CrudRepository<Post, Integer>{
	List<Post> findAll();
}
