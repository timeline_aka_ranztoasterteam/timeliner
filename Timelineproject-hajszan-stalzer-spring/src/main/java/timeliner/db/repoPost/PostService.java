package timeliner.db.repoPost;

import java.util.List;

import timeliner.db.Post;

public interface PostService {
	List<Post> findAll();
	Post save(Post entity);
	void delete(int id);
	Post find(int id);
}
