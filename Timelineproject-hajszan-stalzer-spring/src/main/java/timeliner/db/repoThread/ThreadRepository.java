package timeliner.db.repoThread;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import timeliner.db.Thread;

public interface ThreadRepository extends CrudRepository<Thread, String>{
	List<Thread> findAll();
}
