package timeliner.db.repoThread;

import java.util.List;
import timeliner.db.Thread;

public interface ThreadService {
	List<Thread> findAll();
	Thread save(Thread entity);
	void delete(String id);
	Thread find(String id);
}
