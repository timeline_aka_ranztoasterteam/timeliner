package timeliner.db.repoThread;

import java.util.List;
import timeliner.db.Thread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("threadService")
public class ThreadServiceImpl implements  ThreadService {
	private ThreadRepository threadRepository;
	
	@Autowired
	public ThreadServiceImpl(ThreadRepository threadRepository)
	{
		this.threadRepository = threadRepository;
	}

	@Override
	public List<Thread> findAll() {
		return threadRepository.findAll();
	}
	
	@Override
	public Thread save(Thread entity) {
		return threadRepository.save(entity);
	}
	
	@Override
	public void delete(String id) {
		threadRepository.delete(id);
	}
	
	@Override
	public Thread find(String id) { 
		 return threadRepository.findOne(id);
	}
}