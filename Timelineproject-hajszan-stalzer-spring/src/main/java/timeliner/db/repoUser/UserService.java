package timeliner.db.repoUser;

import java.util.List;

import timeliner.db.User;

public interface UserService {
	List<User> findAll();
	User save(User entity);
	void delete(String id);
	User find(String id);
}
