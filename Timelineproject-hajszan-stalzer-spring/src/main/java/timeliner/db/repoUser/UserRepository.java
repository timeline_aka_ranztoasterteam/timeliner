package timeliner.db.repoUser;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import timeliner.db.User;

public interface UserRepository extends CrudRepository<User, String>{
	List<User> findAll();
}
