package timeliner.db.repoUser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import timeliner.db.User;

@Component("userService")
public class UserServiceImpl implements  UserService {
	private UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository)
	{
		this.userRepository = userRepository;
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}
	
	@Override
	public User save(User entity) {
		return userRepository.save(entity);
	}
	
	@Override
	public void delete(String id) {
		userRepository.delete(id);
	}
	
	@Override
	public User find(String id) { 
		 return userRepository.findOne(id);
	}
}