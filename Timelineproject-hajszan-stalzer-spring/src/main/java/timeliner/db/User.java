package timeliner.db;

import java.beans.Transient;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import timeliner.db.Thread;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "user")
public class User {
	@Id
	@NotNull
	@Column(name = "u_username")
	private String username;

	@NotEmpty(message = "{validation.firstname.NotEmpty.message}")
	@Size(min = 2, max = 60, message = "{validation.firstname.Size.message}")
	@NotNull
	@Column(name = "u_firstname")
	private String firstname;
	
	@NotEmpty(message = "{validation.lastname.NotEmpty.message}")
	@Size(min = 2, max = 60, message = "{validation.lastname.Size.message}")
	@NotNull
	@Column(name = "u_lastname")
	private String lastname;

	@Past
//	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Column(name = "u_birthdate")
	private Date birthdate; 
	
	@NotNull
	@Size(min = 0, max = 200, message="{validation.comment.Length.message}")
	@Column(name = "u_comment")
	private String comment;
	
	@NotNull
	@Email(message = "{validation.email.message}")
	@Column(name = "u_email")
	private String email;
	
	@Column(name = "u_posts")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Post> posts;
	
	@Column(name = "u_threads")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Thread> threads;

	@SuppressWarnings("unused")	// Needed by JPA/Hibernate
	private User() {
	}

	public User(String username, String firstname, String lastname, String email) {
		setUsername(username);
		setFirstname(firstname);
		setLastname(lastname);
		setEmail(email);
		birthdate = Date.valueOf(LocalDate.now());
		posts = new ArrayList<Post>();
		threads = new ArrayList<Thread>();
	}

	public User(String username, String firstname, String lastname, LocalDate birthdate) {
		setUsername(username);
		setFirstname(firstname);
		setLastname(lastname);
		this.birthdate = Date.valueOf(birthdate);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	@Transient
	public String getBirthDateString() {
		String birthDateString = "";
		if (birthdate != null) {
			DateTimeFormatter formatter = DateTimeFormatter
					.ofPattern("dd.MM.yyyy");
			birthDateString = birthdate.toLocalDate().format(formatter);
		}
		return birthDateString;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return username+". "+firstname +" "+lastname +", Geb: "+getBirthDateString() + " "+System.lineSeparator() + " "+getThreadsAndPosts();
	}

	private String getThreadsAndPosts() {
		StringBuilder b = new StringBuilder();
	
		for(Thread t : this.threads)
		{
			b.append(t.getName() + ", ");
		}
		
		return b.toString();
	}

	public String getUsername() {
		return username;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getPosts()
	{
		return posts.size();
	}
	
	public int getThreads()
	{
		return threads.size();
	}
	
	public int getLikes()
	{
		int likes = 0;
	//	for(Post p : posts)
			System.out.println();//implement like in post class 
		
		return likes;
	}
}
