package timeliner.db;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "thread")
public class Thread {
	
	@Id
	@NotNull
	@Column(name = "t_name")
	private String name;
	
	@NotNull
	@Column(name = "t_thematik")
	@Size(min = 3, max = 50, message="{validation.thematik.message}")
	private String thematik;
	
	@Past
	@DateTimeFormat(pattern = "dd.MM.yyyy")
	@Column(name = "t_date")
	private Date date;
	
	@NotNull
	@Column(name="t_wiegeil")
	private int wieGeil;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "u_username", nullable = false)
	private User user;
	
	@Column(name = "t_posts")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "thread")
	private List<Post> posts;
	
	private Thread()
	{
		
	}
	
	
	
	public Thread(String name, String thematik, User user)
	{
		setName(name);
		setThematik(thematik);
		setUser(user);
		setDate(Date.valueOf(LocalDate.now()));
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getThematik() {
		return thematik;
	}

	public void setThematik(String thematik) {
		this.thematik = thematik;
	}

	public int getWieGeil() {
		int good = 0;
		for(Post p : getPosts())
		{
			good+=p.getLikes();
			good-=p.getDislikes();
		}
		wieGeil = good;
		return good; 
	}

	public void setWieGeil(int wieGeil) {
		this.wieGeil = getWieGeil();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	} 
	
	public void setUser(User user)
	{
		this.user = user;
	}
	
	public User getUser()
	{
		return this.user;
	}

	@Override
	public String toString() {
		return "Thread [name=" + name + ", thematik=" + thematik + ", date="
				+ date + ", user=" + user + "]";
	}



	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
	
	
	
}
